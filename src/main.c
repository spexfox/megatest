#include <genesis.h>

#include "../res/gfx.h"
#include "../res/sprite.h"
#include "../res/sound.h"

#define PLAYER_W        24
#define PLAYER_H        48

#define SFX_JUMP        64
#define SFX_ROLL        65
#define SFX_STOP        66

#define ANIM_STAND      0
#define ANIM_WAIT       1
#define ANIM_WALK       2
#define ANIM_RUN        3
#define ANIM_BRAKE      4
#define ANIM_UP         5
#define ANIM_CROUNCH    6
#define ANIM_ROLL       7

#define MAX_SPEED       FIX32(4)
#define RUN_SPEED       FIX32(8)
#define BRAKE_SPEED     FIX32(2)

#define JUMP_SPEED      FIX32(-7)
#define GRAVITY         FIX32(0.3)
#define ACCEL           FIX32(0.2)
#define DE_ACCEL        FIX32(0.80)

#define MIN_POSX        FIX32(10)
#define MAX_POSX        FIX32(400)
#define MAX_POSY        FIX32(152)

#define X_AXIS          0
#define Y_AXIS          1

enum player_states {
    P_ONGROUND = 0,
    P_FALLING = 1,
    P_RISING = 2,
    P_APEX = 3
};

char pState = P_ONGROUND;

typedef struct {
    Vect2D_s16 head;
    Vect2D_s16 body[4];
    Vect2D_s16 feet[2];
    Vect2D_s16 base;
}PlayerCollision;

typedef struct {
    Vect2D_s16 min;
    Vect2D_s16 max;
} RectCollideBox;

// forward
static void handleInput();
static void joyEvent(u16 joy, u16 changed, u16 state);
static void updatePhysic(PlayerCollision* pBox);
static void updateAnim();
static void updateCamera(fix32 x, fix32 y);
static char checkCollision(RectCollideBox *box1, RectCollideBox *box2, char yAxis);

// sprites structure (pointer of Sprite)
Sprite* sprites[3];

// Collision boxes through the level
RectCollideBox collisionData[] = {
    {
        min: {x: 0, y: 200},
        max: {x: 640, y: 232}
    },
    {
        min: {x: 120, y: 160},
        max: {x: 159, y: 175}
    },
    {
        min: {x: 184, y: 136},
        max: {x: 223, y: 151}
    }
};


fix32 camposx;
fix32 camposy;
fix32 posx;
fix32 posy;
fix32 movx;
fix32 movy;
s16 xorder;
s16 yorder;

// fix32 enemyPosx[2];
// fix32 enemyPosy[2];
// s16 enemyXorder[2];

//s16 reseted = TRUE;

char* dbgtext[7];

int main()
{
    u16 palette[64];
    u16 ind;

    // disable interrupt when accessing VDP
    SYS_disableInts();
    // initialization
    VDP_setScreenWidth320();

    // init SFX
    SND_setPCM_XGM(SFX_JUMP, sonic_jump_sfx, sizeof(sonic_jump_sfx));
    SND_setPCM_XGM(SFX_ROLL, sonic_roll_sfx, sizeof(sonic_roll_sfx));
    SND_setPCM_XGM(SFX_STOP, sonic_stop_sfx, sizeof(sonic_stop_sfx));
    // start music
    //SND_startPlay_XGM(sonic_music);

    // init sprites engine
    SPR_init(16, 256, 256);

    // set all palette to black
    VDP_setPaletteColors(0, (u16*) palette_black, 64);

    // load background
    ind = TILE_USERINDEX;
    VDP_drawImageEx(PLAN_B, &bgb_image, TILE_ATTR_FULL(PAL0, FALSE, FALSE, FALSE, ind), 0, 0, FALSE, TRUE);
    ind += bgb_image.tileset->numTile;
    VDP_drawImageEx(PLAN_A, &bga_image, TILE_ATTR_FULL(PAL1, FALSE, FALSE, FALSE, ind), 0, 0, FALSE, TRUE);
    ind += bga_image.tileset->numTile;

    // VDP process done, we can re enable interrupts
    SYS_enableInts();

    camposx = 0;
    camposy = 0;
    posx = FIX32(48);
    posy = MAX_POSY;
    movx = FIX32(0);
    movy = FIX32(0);
    xorder = 0;
    yorder = 0;

    // enemyPosx[0] = FIX32(128);
    // enemyPosy[0] = FIX32(164);
    // enemyPosx[1] = FIX32(300);
    // enemyPosy[1] = FIX32(84);
    // enemyXorder[0] = 1;
    // enemyXorder[1] = -1;

    // init scrolling
    updateCamera(FIX32(0), FIX32(0));

    PlayerCollision _playerBox = {
        head: {x: 25, y: 6},// Top-middle point. Tests head bumps.
        body: {
            {x: 18,y: 18},  // Top-left main hitbox.
            {x: 28,y: 18},  // Top-right main hitbox.
            {x: 18,y: 34},  // Bottom-left main hitbox.
            {x: 28,y: 34}   // Bottom-right main hitbox
        },
        feet: {
            {x: 16,y: 39},  // Left floor edge fall test.
            {x: 32,y: 39}   // Right floor edge fall test.
        },
        base: {x: 25,y: 39} // Actual player position
    };

    // init sonic sprite
    sprites[0] = SPR_addSprite(&sonic_sprite, fix32ToInt(posx - camposx), fix32ToInt(posy - camposy), TILE_ATTR(PAL2, TRUE, FALSE, FALSE));



    // init enemies sprites
    // sprites[1] = SPR_addSprite(&enemies_sprite, fix32ToInt(enemyPosx[0] - camposx), fix32ToInt(enemyPosy[0] - camposy), TILE_ATTR(PAL3, TRUE, FALSE, FALSE));
    // sprites[2] = SPR_addSprite(&enemies_sprite, fix32ToInt(enemyPosx[1] - camposx), fix32ToInt(enemyPosy[1] - camposy), TILE_ATTR(PAL3, TRUE, FALSE, FALSE));
    // select enemy for each sprite
    // SPR_setAnim(sprites[1], 1);
    // SPR_setAnim(sprites[2], 0);
    SPR_update();

    // prepare palettes
    memcpy(&palette[0], bgb_image.palette->data, 16 * 2);
    memcpy(&palette[16], bga_image.palette->data, 16 * 2);
    memcpy(&palette[32], sonic_sprite.palette->data, 16 * 2);
    memcpy(&palette[48], enemies_sprite.palette->data, 16 * 2);

    // fade in
    VDP_fadeIn(0, (4 * 16) - 1, palette, 20, FALSE);

    JOY_setEventHandler(joyEvent);

//    reseted = FALSE;

    while(TRUE)
    {
        handleInput();

        updatePhysic(&_playerBox);
        updateAnim();


//        if (!reseted)
        // update sprites
        SPR_update();

        fix32ToStr(movy, dbgtext,4);
        VDP_drawText(dbgtext,14,24);

        intToStr(pState, dbgtext,1);
        VDP_drawText(dbgtext,14,25);

        intToStr((s32)_playerBox.max.x, dbgtext,4);
        VDP_drawText(dbgtext,24,11);

        intToStr((s32)_playerBox.max.y, dbgtext,4);
        VDP_drawText(dbgtext,24,12);

        // if(checkCollision(&_playerBox, &_testBox))
        // {
        //     VDP_drawText("TOUCHED!",24,7);
        //     sprites[0]
        // }
        // else
        // {
        //     VDP_clearTextLine(7);
        // }

        // if(sprites[0]->x > sprites[1]->x+8 || sprites[0]->x + 48 < sprites[1]->x){
        //     VDP_clearTextLine(7);
        // }
        // else{
        //     VDP_drawText("TOUCHED!",24,7);
        // }

        VDP_waitVSync();
    }

    return 0;
}

static void updatePhysic(PlayerCollision* pBox)
{
    //u16 i;
    // sonic physic
    // ============================================================
    // Apply movements before we check for collisions.
    // ============================================================
    if (xorder > 0)
    {
        movx += ACCEL;
        // going opposite side, quick breaking
        if (movx < 0) movx += ACCEL;

        if (movx >= MAX_SPEED) movx = MAX_SPEED;
    }
    else if (xorder < 0)
    {
        movx -= ACCEL;
        // going opposite side, quick breaking
        if (movx > 0) movx -= ACCEL;

        if (movx <= -MAX_SPEED) movx = -MAX_SPEED;
    }
    else
    {
        if ((movx < FIX32(0.1)) && (movx > FIX32(-0.1)))
            movx = 0;
        else if ((movx < FIX32(0.3)) && (movx > FIX32(-0.3)))
            movx -= movx >> 2;
        else if ((movx < FIX32(1)) && (movx > FIX32(-1)))
            movx -= movx >> 3;
        else
            movx -= movx >> 4;
    }

    posx += movx;

    if(pState)
    {
        movy += GRAVITY;
    }
    
    posy += movy;
    // ============================================================

    pBox->min.x = fix32ToInt(posx);
    pBox->max.x = fix32ToInt(posx) + PLAYER_W;
    pBox->min.y = fix32ToInt(posy);
    pBox->max.y = fix32ToInt(posy) + PLAYER_H;

    char xBoxID = 0;
    char yBoxID = 0;

    char xHit = FALSE;
    char yHit = FALSE;

    for(unsigned char i = 0; i <= 2; i++)
    {

        s16 x1 = collisionData[i].min.x - pBox->max.x;
        s16 y1 = collisionData[i].min.y - pBox->max.y;

        s16 x2 = pBox->min.x - collisionData[i].max.x;
        s16 y2 = pBox->min.y - collisionData[i].max.y;

        if(!(x1 > 0 || y1 > 0) || !(x2 > 0 || y2 > 0))
        {
            if(!xHit && !(x1 > 0 || x2 > 0) && !(y1 > 0 || y2 > 0))
            {
                xHit = TRUE;
                xBoxID = i;
            }

            if(!yHit && !(x1 > 0 || x2 > 0) && !(y1 > 0 || y2 > 0))
            {
                yHit = TRUE;
                yBoxID = i;
            }
        }
    }

    // if(xHit)
    // {
    //     s16 x1 = collisionData[xBoxID].min.x - pBox->max.x;
    //     s16 y1 = collisionData[xBoxID].min.y - pBox->max.y;

    //     s16 x2 = pBox->min.x - collisionData[xBoxID].max.x;
    //     s16 y2 = pBox->min.y - collisionData[xBoxID].max.y;

    //     if(movx > 0)
    //     {
    //         posx += intToFix32(x1);
    //         pBox->min.x = fix32ToInt(posx);
    //         pBox->max.x = fix32ToInt(posx) + PLAYER_W;
    //     }
    //     else if(movx < 0)
    //     {
    //         posx += intToFix32(x2);
    //         pBox->min.x = fix32ToInt(posx);
    //         pBox->max.x = fix32ToInt(posx) + PLAYER_W;
    //     }
    // }

    if(yHit)
    {
        s16 x1 = collisionData[yBoxID].min.x - pBox->max.x;
        s16 y1 = collisionData[yBoxID].min.y - pBox->max.y;

        s16 x2 = pBox->min.x - collisionData[yBoxID].max.x;
        s16 y2 = pBox->min.y - collisionData[yBoxID].max.y;

        intToStr(yBoxID, dbgtext,1);
        VDP_drawText(dbgtext,14,23);

        if(pState == P_ONGROUND)
        {
            if(!(y1 > 0) && !(x1 > 0 || x2 > 0))
            {
                pState = P_ONGROUND;
                posy += intToFix32(y1);
                movy = 0;
                pBox->min.y = fix32ToInt(posy);
                pBox->max.y = fix32ToInt(posy) + PLAYER_H;
            }
            else if(x1 > 0 || x2 > 0)
            {
                movy += GRAVITY;
                pState = P_FALLING;
            }
            else
            {
                pState = P_RISING;
            }            
        }
        else
        {
            if(movy < 0)
            {
                if(!(y1 > 0))
                {
                    posy += intToFix32(y2);
                    movy = -movy;
                }
                else
                {
                    pState = P_RISING;
                }
                
            }
            else if(movy == 0)
            {
                pState = P_APEX;
            }
            else if(movy > 0)
            {
                if(!(y1 > 0))
                {
                    posy += intToFix32(y1);
                    pState = P_ONGROUND;     
                }
                else
                {
                    pState = P_FALLING;
                }
                
            }
        }
    }
    else if(!yHit && !pState)
    {
        movy += GRAVITY;
        pState = P_FALLING;
    }

    pBox->min.y = fix32ToInt(posy);
    pBox->max.y = fix32ToInt(posy) + PLAYER_H;

    pBox->min.x = fix32ToInt(posx);
    pBox->max.x = fix32ToInt(posx) + PLAYER_W;

    fix32 px_scr, py_scr;
    fix32 npx_cam, npy_cam;

    // get sprite position on screen
    px_scr = posx - camposx;
    py_scr = posy - camposy;

    // calculate new camera position
    if (px_scr > FIX32(240)) npx_cam = posx - FIX32(240);
    else if (px_scr < FIX32(40)) npx_cam = posx - FIX32(40);
    else npx_cam = camposx;
    if (py_scr > FIX32(160)) npy_cam = posy - FIX32(160);
    else if (py_scr < FIX32(100)) npy_cam = posy - FIX32(100);
    else npy_cam = camposy;

    // clip camera position
    if (npx_cam < FIX32(0)) npx_cam = FIX32(0);
    else if (npx_cam > FIX32(200)) npx_cam = FIX32(200);
    if (npy_cam < FIX32(0)) npy_cam = FIX32(0);
    else if (npy_cam > FIX32(100)) npy_cam = FIX32(100);

    // set camera position
    updateCamera(npx_cam, npy_cam);

    // set sprites position
    SPR_setPosition(sprites[0], fix32ToInt(posx - camposx), fix32ToInt(posy - camposy));

}

static void updateAnim()
{
    // jumping
    if (movy) SPR_setAnim(sprites[0], ANIM_ROLL);
    else
    {
        if (((movx >= BRAKE_SPEED) && (xorder < 0)) || ((movx <= -BRAKE_SPEED) && (xorder > 0)))
        {
            if (sprites[0]->animInd != ANIM_BRAKE)
            {
                SND_startPlayPCM_XGM(SFX_STOP, 1, SOUND_PCM_CH2);
                SPR_setAnim(sprites[0], ANIM_BRAKE);
            }
        }
        else if ((movx >= RUN_SPEED) || (movx <= -RUN_SPEED))
            SPR_setAnim(sprites[0], ANIM_RUN);
        else if (movx != 0)
            SPR_setAnim(sprites[0], ANIM_WALK);
        else
        {
            if (yorder < 0)
                SPR_setAnim(sprites[0], ANIM_UP);
            else if (yorder > 0)
                SPR_setAnim(sprites[0], ANIM_CROUNCH);
            else
                SPR_setAnim(sprites[0], ANIM_STAND);
        }
    }

    if (movx > 0) SPR_setHFlip(sprites[0], FALSE);
    else if (movx < 0) SPR_setHFlip(sprites[0], TRUE);

}

static void updateCamera(fix32 x, fix32 y)
{
    if ((x != camposx) || (y != camposy))
    {
        camposx = x;
        camposy = y;
        VDP_setHorizontalScroll(PLAN_A, fix32ToInt(-camposx));
        VDP_setHorizontalScroll(PLAN_B, fix32ToInt(-camposx) >> 3);
        VDP_setVerticalScroll(PLAN_A, fix32ToInt(camposy));
        VDP_setVerticalScroll(PLAN_B, fix32ToInt(camposy) >> 3);
    }
}

//axis argument is binary option between X_AXIS or Y_AXIS
static char checkCollision(RectCollideBox* box1, RectCollideBox* box2, char yAxis)
{
    // if(yAxis)
    // {
        if(box1->min.y > box2->max.y || box1->max.y < box2->min.y)
        {
            return FALSE;
        }
        // else
        // {
        //     return TRUE;
        // }
    // }
    // else
    // {
        else if(box1->min.x > box2->max.x || box1->max.x < box2->min.x)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    // }

}

static void handleInput()
{
    u16 value = JOY_readJoypad(JOY_1);

    if (value & BUTTON_UP) yorder = -1;
    else if (value & BUTTON_DOWN) yorder = +1;
    else yorder = 0;

    if (value & BUTTON_LEFT) xorder = -1;
    else if (value & BUTTON_RIGHT) xorder = +1;
    else xorder = 0;

//    if (value & BUTTON_START)
//    {
//        SPR_reset();
//        SPR_clear();
//
//        VDP_waitVSync();
//
//        sprites[0] = SPR_addSprite(&sonic_sprite, 128, 128, TILE_ATTR(PAL2, TRUE, FALSE, FALSE));
//        reseted = TRUE;
//    }
}

static void joyEvent(u16 joy, u16 changed, u16 state)
{
    // START button state changed
    if (changed & BUTTON_START)
    {

    }

    if (changed & state & (BUTTON_A | BUTTON_B | BUTTON_C))
    {
        // if (movy == 0)
        if (!pState)
        {
            pState = P_RISING;
            movy = JUMP_SPEED;
            SND_startPlayPCM_XGM(SFX_JUMP, 1, SOUND_PCM_CH2);
        }
    }
}
